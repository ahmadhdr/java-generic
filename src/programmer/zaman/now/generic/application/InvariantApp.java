package programmer.zaman.now.generic.application;

import programmer.zaman.now.generic.MyData;

public class InvariantApp {
    public static void main(String[] args) {
        /**
         * tidak ada hubungan polymoph di dalam generic
         * by default generic adalah invariant
         */
        MyData<String> name = new MyData<>("haidar");
        System.out.println(name.getData());

//        doit(name); // error because invariant
    }

    public static void doit(MyData<Object> data) {
        // do nothin
    }

}
