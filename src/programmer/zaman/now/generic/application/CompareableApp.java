package programmer.zaman.now.generic.application;

import programmer.zaman.now.generic.Person;

import java.util.Arrays;

public class CompareableApp {
    public static void main(String[] args) {
        Person[] people = {
                new Person("haidar","mekarjati"),
                new Person("haidar","mekarjati"),
                new Person("haidar","mekarjati"),
        };

        Arrays.sort(people);
        System.out.println(Arrays.toString(people));
    }
}
