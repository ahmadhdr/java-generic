package programmer.zaman.now.generic.application;

import programmer.zaman.now.generic.utils.ArrayHelper;

public class ArrayHelperApp {
    public static void main(String[] args) {
        String[] names = {"ahmad","haidar" ,"albaqir"};
        Integer[] numbers = {1,2,3,4};

        System.out.println(
                ArrayHelper.<String>count((names))
        );

        System.out.println(ArrayHelper.count(numbers));

    }
}
