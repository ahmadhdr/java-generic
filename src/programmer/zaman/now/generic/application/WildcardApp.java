package programmer.zaman.now.generic.application;

import programmer.zaman.now.generic.MyData;

public class WildcardApp {
    public static void main(String[] args) {
        print(new MyData<Integer>(100));
        print(new MyData<String>("haidar"));
        print(new MyData<MultipleBoundedType.Manager>(new MultipleBoundedType.Manager()));

    }

    public static void print(MyData<?> data) {
        System.out.println(data.getData());
    }
}
