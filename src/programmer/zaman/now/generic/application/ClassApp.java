package programmer.zaman.now.generic.application;

import programmer.zaman.now.generic.MyData;

public class ClassApp {
    public static void main(String[] args) {
        /**
         * Cara pembuatannya tinggal di ganti parameter type nya apa
         * - generic bisa lebih dari 1 tapi nama generic nya harus lebih dari 1
         */
        MyData<String> name = new MyData<String>("Haidar");
        MyData<Integer> age = new MyData<Integer>(1);

        System.out.println(age.getData());
        System.out.println(name.getData());
    }
}
