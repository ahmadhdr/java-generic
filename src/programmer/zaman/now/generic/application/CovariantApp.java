package programmer.zaman.now.generic.application;

import programmer.zaman.now.generic.MyData;

public class CovariantApp {
    public static void main(String[] args) {
        MyData<String> name = new MyData<>("haidar");
    }

    /**
     * @desc ini dinamakan covariant, pada covarian kita hanya bisa mengakses datanya saja dan tidak bisa
     * merubahnya ,karena covarian hanya bisa mengakses method yang type nya generic tapi sedangkan untuk
     * method yang parameter nya generic tidak bisa.
     * alasanya karena ketika covariant bisa merubah data di khawatirkan akan merubah tipe data yang sudah di deklarsikan
     * sejak awal, kalau kurang paham tonton yt programmer zaman now lagi
     */
    public static void process(MyData<? extends Object> data) {
        data.getData();

        // data.setData(1); tidak boleh karena berbahaya
    }
}
