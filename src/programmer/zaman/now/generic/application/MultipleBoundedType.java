package programmer.zaman.now.generic.application;

public class MultipleBoundedType {
    public static void main(String[] args) {
//        Data<Manager> managerData = new Data<>(new Manager()); error karena manager tidak implement can say hello
        Data<VicePresident> vicePresidentData = new Data<>(new VicePresident());
    }

    public static interface CanSayHello {
        void sayHello(String name);
    }

    public static abstract  class Employee {

    }

    public static class Manager extends  Employee {

    }

    public static class VicePresident extends  Employee implements  CanSayHello {
        public void sayHello(String data) {
            System.out.println("Hello : " + data);
        }
    }

    public static class Data<T extends Employee & CanSayHello> {
        private T data;

        public Data(T data) {
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

    }
}
