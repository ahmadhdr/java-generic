package programmer.zaman.now.generic.application;

public class BoundedTypeApp {
    public static void main(String[] args) {
        /**
         * - Kadang kita ingin membatasi data yang boleh digunaka digeneric type
         * - kita boleh menambahkan constraint di generic type, dengan cara menyebut secara spesifik type yang diperbolehka
         * - secara otomatis data yang boleh digunakan adalah data yang sudah disebutkan atau pun turunanya
         * - secara default, constraint type untuk generic parameter adalah object, sehingga semua tipe bisa dimasukan
         */

        NumberData<Integer> data = new NumberData<>(1);
        System.out.println(data.getData());

    }

    /**
     * untuk multipe bounded type bisa menambahkan tanda dan setelah generic type yang pertama tapi
     * untuk generic type yang kedua dan ketiga harus berupa interface karena simple dijava class tidak boleh mempunyai parent
     * dari 1
     */
    public static class NumberData<T extends  Number> {
        private  T data;

        public NumberData(T data) {
            this.data = data;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }
    }
}
