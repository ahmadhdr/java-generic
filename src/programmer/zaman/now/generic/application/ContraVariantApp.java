package programmer.zaman.now.generic.application;

import programmer.zaman.now.generic.MyData;

public class ContraVariantApp {
    public static void main(String[] args) {
        // kenapa contra variant tidak aman ketika melakukan get data karena generic type Object yang dimana semua class adalah child dari class Object

        MyData<Object> objectMyData = new MyData<>("eko");
        MyData<? super String> myData = objectMyData;

        process(myData);
        System.out.println(objectMyData.getData());
    }

    public static void process(MyData<? super String> myData) {
        String value  = (String) myData.getData();
        System.out.println("Process data =>" + value );
        myData.setData("Eko kurniawan khannedy");
    }
}
