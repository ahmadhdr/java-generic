package programmer.zaman.now.generic;

public class MyData<T> {

    /**
     * T = type
     * K = key
     * V = value
     */

    private T data;

    public MyData(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
